from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=100)

class Member(models.Model):
    name = models.CharField(max_length=100)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='members')

class Album(models.Model):
    name = models.CharField(max_length=100)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='albums')
    release_date = models.DateField(blank=True, null=True)

class Version(models.Model):
    name = models.CharField(max_length=100)
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='version')


class Photocard(models.Model):

    # Basic Information
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='photocards')
    member = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='photocards')
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='photocards')  # Inserted here
    version = models.ForeignKey(Version, on_delete=models.CASCADE, related_name='photocards')

    # Physical Characteristics
    material = models.CharField(
        max_length=255,
        choices=[
            ('paper', 'Paper'),
            ('plastic', 'Plastic'),
            ('lenticular', 'Lenticular'),
            # Add more options as needed
        ],
        blank=True,
    )
    corner_shape = models.CharField(
        max_length=255,
        choices=[
            ('square', 'Square'),
            ('rounded', 'Rounded'),
        ],
        blank=True,
    )

    # Acquisition and Condition
    store = models.CharField(max_length=255, blank=True)  # Changed obtained_from to store
    is_pob = models.BooleanField(default=False)  # Added field for POB checkmark
    is_holographic = models.BooleanField(default=False)  # Added holographic checkbox

    # Status Dropdown
    STATUS_CHOICES = [
        ('owned', 'Owned'),
        ('on_the_way', 'On the way'),
        ('need', 'Need'),
    ]
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='need')

    # Additional Information
    notes = models.TextField(blank=True)
    front_image = models.ImageField(upload_to='photocards/', blank=True)
    back_image = models.ImageField(upload_to='photocards/', blank=True)

    def __str__(self):
        return f"{self.group} - {self.member} ({self.album.name} - {self.version.name}) {'[POB]' if self.is_pob else ''} (Material: {self.material}, Corner: {self.corner_shape})"

class Collection(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='collections')
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    is_public = models.BooleanField(default=False)

class CollectionItem(models.Model):
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE, related_name='items')
    photocard = models.ForeignKey(Photocard, on_delete=models.CASCADE, related_name='collections')
    quantity = models.PositiveIntegerField(default=1)
