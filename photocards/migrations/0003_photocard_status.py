# Generated by Django 5.0.2 on 2024-02-15 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "photocards",
            "0002_remove_photocard_condition_photocard_corner_shape_and_more",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="photocard",
            name="status",
            field=models.CharField(
                choices=[
                    ("owned", "Owned"),
                    ("on_the_way", "On the way"),
                    ("need", "Need"),
                ],
                default="need",
                max_length=20,
            ),
        ),
    ]
